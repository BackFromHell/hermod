#!/usr/bin/python3
# -*- coding : utf-8 -*-

import pprint

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QBrush, QColor, QFont

from PyQt5.QtCore import Qt

from imap_tools import MailBox, AND
import json

import re
import os

class HMailbox(object):
    """docstring for Mailbox."""

    def __init__(self, MailBoxName, MailBoxServer, MailBoxUser, MailBoxPassword):
        super(HMailbox, self).__init__()
        self.MailBoxName= MailBoxName
        self.MailBoxServer= MailBoxServer
        self.MailBoxUser= MailBoxUser
        self.MailBoxPassword= MailBoxPassword
        self.MailBoxType= 'IMAP4'
    def saveCacheFile(self):
        with open(os.path.join(os.path.expanduser('~'), '.hermod', '.cache', self.MailBoxName + '-folders.cache'), 'w') as outfile:
            json.dump(self.getFolders(), outfile)
    def readCacheFile(self):
        with open(os.path.join(os.path.expanduser('~'), '.hermod', '.cache', self.MailBoxName + '-folders.cache')) as json_file:
            return(json.load(json_file))
    def getMailboxFolders(self):
        return({'mailboxtype':'IMAP4', 'itemtype':'mailbox', 'name':self.MailBoxName, 'Children' : self.getFolders()})
    def getFolders(self):
        self.MailboxConnector = MailBox(self.MailBoxServer)
        self.MailboxConnector.login(self.MailBoxUser, self.MailBoxPassword)
        CacheFolders=[]
        #| Read first folder level.
        Folders = self.MailboxConnector.folder.list()
        firstLevelFolders = []
        for Folder in Folders:
            if (len(Folder['name'].split(Folder['delim'])) == 1):
                firstLevelFolders.append(Folder['name'])
        sortedfirstLevelFolders = ['INBOX', 'Drafts', 'Queue', 'Trash']
        for Folder in sortedfirstLevelFolders:
            if (firstLevelFolders.index(Folder)):
                firstLevelFolders.remove(Folder)
                firstLevelFolders.insert(sortedfirstLevelFolders.index(Folder), Folder)

        for firstLevelFolder in firstLevelFolders:
            for subFolder in self.MailboxConnector.folder.list(firstLevelFolder):
                Parents=subFolder['name'].split(subFolder['delim'])[:-1]
                FolderStatistics = self.MailboxConnector.folder.status(subFolder['name'])
                tmpDict={'name': subFolder['name'].split(subFolder['delim'])[-1],
                         'itemtype':'folder',
                         'Marked':'False',
                         'Total':FolderStatistics['MESSAGES'],
                         'New':FolderStatistics['RECENT'],
                         'Unread':FolderStatistics['UNSEEN'],
                         'Children': []
                        }
                if ('\\Marked' in subFolder['flags']):
                    FolderStatistics['Marked'] = True
                if not (len(Parents)):
                  CacheFolders.append(tmpDict)
                else:
                  Cursor = CacheFolders
                  for Parent in Parents:
                      for curFolder in Cursor:
                          if (Parent == curFolder['name']):
                              Cursor = curFolder['Children']
                              break
                  Cursor.append(tmpDict)

        self.MailboxConnector.logout()
        return(CacheFolders)
class TreeWidgetItem(QTreeWidgetItem):
    """docstring for TreeWidgetItem."""

    def __init__(self, txt='', itemtype='', parent=None, font_size=9, set_bold=False, color=Qt.black, iconname=''):
        super().__init__(parent)
        fnt = QFont('Open Sans', font_size)
        fnt.setBold(set_bold)
        self.setForeground(0, QBrush(color))
        self.setFont(0, fnt)
        self.setText(0, txt)
        self.setText(1, itemtype)
        self.setIcon(0,QIcon(iconname))
class MailboxTreeWidget(QTreeWidget):
    """docstring for MailboxTreeWidget."""

    def __init__(self, columncount = 2, headerslabels = ['Mailboxes', 'Itemtype'], headerhidden = True, MailBoxes = []):
        super().__init__()

        self.MailBoxes = MailBoxes

        #Set the number of columns
        self.setColumnCount(columncount)
        #Set the title of the tree control head
        self.setHeaderLabels(headerslabels)
        self.setHeaderHidden(headerhidden)
        self.hideColumn(1)
        #Load all properties and sub-controls of the root node
        self.addTopLevelItem(self.LoadTreeFromCache(self))

        #TODO Optimization 3 Add response events to nodes
        self.clicked.connect(self.onClicked)

        #Node Expand All
        self.expandAll()

    def contextMenuEvent(self, event):
        """ ContextMenuPolicy --> DefaultContextMenu """
        #print(event.pos())
        #print(self.mapToGlobal(event.pos()))
        item=self.currentItem()
        #print('Key=%s, Itemtype=%s'%(item.text(0),item.text(1)))

        refreshAct = QAction("&Refresh")
        refreshAct.setStatusTip("Refresh MailBox")
        refreshAct.triggered.connect( self.RefreshMailbox )

        newFolderAct = QAction("&New folder")
        newFolderAct.setStatusTip("Create a new folder")
        newFolderAct.triggered.connect( self.NewFolder )

        renameFolderAct = QAction("&Rename folder")
        renameFolderAct.setStatusTip("Rename the folder")
        renameFolderAct.triggered.connect( self.RenameFolder )

        copyFolderAct = QAction("&Copy")
        copyFolderAct.setStatusTip("Copy folder")
        copyFolderAct.triggered.connect( self.CopyFolder )

        moveFolderAct = QAction("&Move folder")
        moveFolderAct.setStatusTip("Move the folder")
        moveFolderAct.triggered.connect( self.MoveFolder )

        deleteFolderAct = QAction("&Delete folder")
        deleteFolderAct.setStatusTip("Delete the folder")
        deleteFolderAct.triggered.connect(self.DeleteFolder )

        markasreadAct = QAction("Mark as &read")
        markasreadAct.setStatusTip("Mark all mails in the folder as read")
        markasreadAct.triggered.connect( self.MarkasRead )

        emptyFolderAct = QAction("&Empty the folder")
        emptyFolderAct.setStatusTip("Delete all mails in the folder")
        emptyFolderAct.triggered.connect( self.EmptyFolder )

        propertiesAct = QAction("&Properties")
        propertiesAct.setStatusTip("Mailbox's properties")
        propertiesAct.triggered.connect(self.Properties)

        contextMenu = QMenu(self)
        if (item.text(1) == "Mailbox"):
            contextMenu.addAction(refreshAct)
            contextMenu.addSeparator()
            contextMenu.addAction(newFolderAct)
            contextMenu.addSeparator()
            contextMenu.addAction(propertiesAct)
        elif  (item.text(1) == "Folder"):
            contextMenu.addAction(newFolderAct)
            contextMenu.addSeparator()
            contextMenu.addAction(renameFolderAct)
            contextMenu.addAction(copyFolderAct)
            contextMenu.addAction(moveFolderAct)
            contextMenu.addAction(deleteFolderAct)
            contextMenu.addSeparator()
            contextMenu.addAction(markasreadAct)
            contextMenu.addAction(emptyFolderAct)

        action = contextMenu.exec_(self.mapToGlobal(event.pos()))

    def LoadTreeFromCache(self, Parent):
        for MailBox in self.MailBoxes:
            rootNode=TreeWidgetItem(MailBox.MailBoxName + ' (' + MailBox.MailBoxType + ')', 'Mailbox', Parent, iconname='c:/Asgardian.be/MailReader/ico/Mailbox.png')
            self.addFolders(MailBox.readCacheFile(), rootNode)
    def addFolders(self, Folders, MailBoxTree):
        for Folder in Folders:
            ItemName = Folder['name']
            Color=Qt.black
            Set_bold=False
            if Folder['Unread']:
                ItemName += ' (' +  str(Folder['Unread']) + ')'
                Color=Qt.red
            #if (Folder['Marked']):
                #Set_bold=True
            Node=TreeWidgetItem(ItemName, 'Folder', MailBoxTree, set_bold=Set_bold, color=Color)
            if (len(Folder['Children'])):
                self.addFolders(Folder['Children'], Node)
    def cleanTree(self):
        iterator = QTreeWidgetItemIterator(self, QTreeWidgetItemIterator.All)
        while iterator.value():
            iterator.value().takeChildren()
            iterator += 1
        i = self.topLevelItemCount()
        while i > -1:
            self.takeTopLevelItem(i)
            i -= 1
    def refreshTree(self):
        self.cleanTree()
        #Load all properties and sub-controls of the root node
        self.addTopLevelItem(self.LoadTreeFromCache(self))
        #Node Expand All
        self.expandAll()
    def onClicked(self,qmodeLindex):
        item=self.currentItem()
        print('Key=%s,value=%s'%(item.text(0),item.text(1)))
    def FindMailbox(self, MailBoxes):
        item = self.currentItem()
        for MailBox in MailBoxes:
            if (MailBox.MailBoxName == (re.search('^(.*)\s\(.*\)', item.text(0))).group(1)):
                return(MailBox)
    def RefreshMailbox(self):
        MailBox = self.FindMailbox(self.MailBoxes)
        MailBox.saveCacheFile()
        self.refreshTree()
    def NewFolder(self):
        #MailBox = self.FindMailbox(self.MailBoxes)
        item = self.currentItem()
        print("-- %s" % item.text(0))

        Node=TreeWidgetItem('Folder', 'Folder', item, set_bold=False)
        self.setCurrentItem(root)

        #delegate = QStyledItemDelegate()
        #self.setItemDelegate(delegate)


        #self.openPersistentEditor(Node)

        #self.closePersistentEditor(item)
    def RenameFolder(self):
        pass
    def CopyFolder(self):
        pass
    def MoveFolder(self):
        pass
    def DeleteFolder(self):
        pass
    def MarkasRead(self):
        pass
    def EmptyFolder(self):
        pass
    def Properties(self):
        pass

class listMessagesWidget(QTreeWidget):
    """docstring for listMessagesWidget."""

    def __init__(self, columncount = 4, headerslabels = ['Subject', 'Sender', 'Date', 'Size'], headerhidden = False):

        super().__init__()

        #Set the number of columns
        self.setColumnCount(columncount)
        #Set the title of the tree control head
        self.setHeaderLabels(headerslabels)
        self.setHeaderHidden(headerhidden)

class HermodMainWindows(QMainWindow):
    def __init__(self, parent=None, MailBoxes=[]):
        super(HermodMainWindows, self).__init__(parent)
        self.MailBoxes = MailBoxes
        self.setContentsMargins(5,5,5,5)
        self.setWindowTitle('Hærmóðr - The mail client')
        self.setWindowIcon(QIcon(os.path.join(os.path.dirname(__file__), 'ico', 'Hermod.png')))
        self.resize(1000, 900)

        #| The menuBar
        #  ----------------------------------------------------------------
        exitAct = QAction('&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)

        #| The toolBar
        #  ----------------------------------------------------------------
        RefreshMailboxAct = QAction(QIcon(os.path.join(os.path.dirname(__file__), 'ico', 'Download.ico')), '&Refresh Mailbox', self)
        RefreshMailboxAct.setStatusTip('Refresh MailBox')
        RefreshMailboxAct.triggered.connect(self.RefreshMailboxes)
        self.toolbar = self.addToolBar('Refresh Mailbox')
        self.toolbar.addAction(RefreshMailboxAct)

        #| The statusBar
        #  ----------------------------------------------------------------
        self.statusBar()

        self.messageView = QTextBrowser()
        self.mailboxesTree = MailboxTreeWidget(MailBoxes=self.MailBoxes)
        self.listMessages = listMessagesWidget()
        self.messageSplitter = QSplitter(Qt.Vertical)
        self.messageSplitter.addWidget(self.listMessages)
        self.messageSplitter.addWidget(self.messageView)
        self.mainSplitter = QSplitter(Qt.Horizontal)
        self.mainSplitter.addWidget(self.mailboxesTree)
        self.mainSplitter.addWidget(self.messageSplitter)
        self.setCentralWidget(self.mainSplitter)
        self.mainSplitter.setSizes([100,200])

    def setMailBoxes(self, MailBoxes):
        self.MailBoxes = MailBoxes
    def RefreshMailboxes(self):
        for cMailBox in self.MailBoxes:
            cMailBox.saveCacheFile()
        self.mailboxesTree.refreshTree()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    IMAP = HMailbox('thierry.leurent@asgardian.be', 'ssl0.ovh.net', 'thierry.leurent@asgardian.be', "TLd0n'tre4d")

    MailBoxes = []
    MailBoxes.append(IMAP)

    MainWindows = HermodMainWindows(MailBoxes=MailBoxes)

    MainWindows.show()
    sys.exit(app.exec_())
